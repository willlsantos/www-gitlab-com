---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 1 | 1080 |
| [@engwan](https://gitlab.com/engwan) | 2 | 680 |
| [@manojmj](https://gitlab.com/manojmj) | 3 | 560 |
| [@alexpooley](https://gitlab.com/alexpooley) | 4 | 500 |
| [@pks-t](https://gitlab.com/pks-t) | 5 | 500 |
| [@theoretick](https://gitlab.com/theoretick) | 6 | 400 |
| [@whaber](https://gitlab.com/whaber) | 7 | 400 |
| [@djadmin](https://gitlab.com/djadmin) | 8 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 9 | 400 |
| [@10io](https://gitlab.com/10io) | 10 | 380 |
| [@mksionek](https://gitlab.com/mksionek) | 11 | 340 |
| [@mrincon](https://gitlab.com/mrincon) | 12 | 340 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 13 | 320 |
| [@sabrams](https://gitlab.com/sabrams) | 14 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 15 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 16 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 17 | 300 |
| [@leipert](https://gitlab.com/leipert) | 18 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 19 | 250 |
| [@tkuah](https://gitlab.com/tkuah) | 20 | 240 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 21 | 230 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 22 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 23 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 24 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 25 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 26 | 200 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 27 | 200 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 28 | 140 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 29 | 140 |
| [@kerrizor](https://gitlab.com/kerrizor) | 30 | 130 |
| [@twk3](https://gitlab.com/twk3) | 31 | 130 |
| [@seanarnold](https://gitlab.com/seanarnold) | 32 | 120 |
| [@balasankarc](https://gitlab.com/balasankarc) | 33 | 110 |
| [@stanhu](https://gitlab.com/stanhu) | 34 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 35 | 100 |
| [@cablett](https://gitlab.com/cablett) | 36 | 100 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 37 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 38 | 80 |
| [@splattael](https://gitlab.com/splattael) | 39 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 40 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 41 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 42 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 43 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 44 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 45 | 80 |
| [@mkozono](https://gitlab.com/mkozono) | 46 | 80 |
| [@dblessing](https://gitlab.com/dblessing) | 47 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 48 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 49 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 50 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 51 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 52 | 50 |
| [@tomquirk](https://gitlab.com/tomquirk) | 53 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 54 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 55 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 56 | 40 |
| [@dgruzd](https://gitlab.com/dgruzd) | 57 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 58 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 59 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 60 | 30 |
| [@cngo](https://gitlab.com/cngo) | 61 | 30 |
| [@ekigbo](https://gitlab.com/ekigbo) | 62 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 63 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 80 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |
| [@rspeicher](https://gitlab.com/rspeicher) | 8 | 30 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |
| [@tnir](https://gitlab.com/tnir) | 2 | 200 |

## FY22-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@pks-t](https://gitlab.com/pks-t) | 1 | 500 |
| [@manojmj](https://gitlab.com/manojmj) | 2 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 3 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 4 | 400 |
| [@10io](https://gitlab.com/10io) | 5 | 380 |
| [@mrincon](https://gitlab.com/mrincon) | 6 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 7 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 8 | 300 |
| [@tkuah](https://gitlab.com/tkuah) | 9 | 240 |
| [@mksionek](https://gitlab.com/mksionek) | 10 | 200 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 11 | 200 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 12 | 120 |
| [@mkozono](https://gitlab.com/mkozono) | 13 | 80 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 14 | 80 |
| [@dblessing](https://gitlab.com/dblessing) | 15 | 80 |
| [@leipert](https://gitlab.com/leipert) | 16 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 17 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 18 | 60 |
| [@jerasmus](https://gitlab.com/jerasmus) | 19 | 60 |
| [@dgruzd](https://gitlab.com/dgruzd) | 20 | 40 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 21 | 40 |
| [@kerrizor](https://gitlab.com/kerrizor) | 22 | 30 |
| [@ekigbo](https://gitlab.com/ekigbo) | 23 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 24 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rspeicher](https://gitlab.com/rspeicher) | 1 | 30 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 2 | 30 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@tnir](https://gitlab.com/tnir) | 1 | 200 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 1 | 1000 |
| [@engwan](https://gitlab.com/engwan) | 2 | 680 |
| [@alexpooley](https://gitlab.com/alexpooley) | 3 | 500 |
| [@theoretick](https://gitlab.com/theoretick) | 4 | 400 |
| [@whaber](https://gitlab.com/whaber) | 5 | 400 |
| [@sabrams](https://gitlab.com/sabrams) | 6 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 7 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 8 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 9 | 250 |
| [@leipert](https://gitlab.com/leipert) | 10 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 11 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 12 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 13 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 14 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 15 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 16 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 17 | 140 |
| [@twk3](https://gitlab.com/twk3) | 18 | 130 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 19 | 110 |
| [@balasankarc](https://gitlab.com/balasankarc) | 20 | 110 |
| [@stanhu](https://gitlab.com/stanhu) | 21 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 22 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 23 | 100 |
| [@cablett](https://gitlab.com/cablett) | 24 | 100 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 25 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 26 | 80 |
| [@splattael](https://gitlab.com/splattael) | 27 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 28 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 29 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 30 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 31 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 32 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 33 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 34 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 35 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 36 | 60 |
| [@manojmj](https://gitlab.com/manojmj) | 37 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 38 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 39 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 40 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 41 | 50 |
| [@tomquirk](https://gitlab.com/tomquirk) | 42 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 43 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 44 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 45 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 46 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 47 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 48 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 49 | 30 |
| [@cngo](https://gitlab.com/cngo) | 50 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 50 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


