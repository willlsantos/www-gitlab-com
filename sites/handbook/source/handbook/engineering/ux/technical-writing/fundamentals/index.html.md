---
layout: handbook-page-toc
title: "Technical Writing Fundamentals"
description: "Get started working with GitLab product documentation with our Technical Writing Fundamentals."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Technical Writing Fundamentals course is available to help GitLab and community contributors document product features. This course provides direction on grammar, tooling, and topic design. 

## Technical Writing Fundamentals course

The Technical Writing Fundamentals course consists of the following sections to be completed in order. 

### Google Technical Writing One pre-class material

Complete the pre-class exercises from this excellent Google training. You can take the associated Google class if you like, but the GitLab Technical Writing Fundamentals sessions 1-4 cover the information that is relevant for GitLab.

[**Start with the pre-class work** ~12 short sessions](https://developers.google.com/tech-writing/one).

### GitLab Technical Writing Fundamentals slide deck
Use the slide deck to follow along with the recorded training sessions.

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRGaE-gaI_couZMS1WJSAT0o8EGeFrZbrEevN-Z7bXBS1MxumUTk4c1ouERsGUEE0fhbofDY6BWLUIN/embed?start=false&loop=false&delayms=5000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

### Session 1 

Provides an introduction, the audience for the documentation, a brief recap of pre-class work, and the first grammar and style requirements. _Video: coming soon!_
  
### Session 2

Reviews additional grammar and style requirements. _Video: coming soon!_
  
### Session 3

Reviews even more grammar and style requirements and linting. _Video: coming soon!_

### Session 4

Reviews topic types: concepts, tasks, references, troubleshooting (CTRT). _Video: coming soon!_

## Schedule in-person training

While we emphasize asynchronous training, in-person training for GitLab team members is available on a limited basis. Contact Susan Tacker (`@susantacker` in Slack) to schedule training sessions.
